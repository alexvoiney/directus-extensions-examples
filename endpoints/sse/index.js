/**
 * Custom endpoint /custom/sse/
 *
 * Will handle webhook on /custom/sse/emit/
 * Stream Server-Sent Event.
 */
const EventEmmiter = require("events"); // lint

// Create the stream object
const Stream = new EventEmmiter();

module.exports = function registerEndpoint(router) {
  router.get("/", (req, res) => {
    res.writeHead(200, {
      "Content-Type": "text/event-stream",
      "Cache-Control": "no-cache",
      Connection: "keep-alive", // keep the connection open to client
    });

    // Each time the server emit and push on message,
    // write data through the connection.
    Stream.on("push", function (event, data) {
      res.write(JSON.stringify(data));
    });
  });

  router.post("/emit/", (req, res) => {
    // Send the body of the event to the stream
    Stream.emit("push", "message", req.body);
    // return 201 Accepted for the POST request
    res.writeHead(201, {
      "Content-Type": "application/json",
    });
    res.end();
  });
};
